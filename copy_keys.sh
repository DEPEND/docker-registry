#! /bin/bash

CERTDIR="/scratch/docker-registry/certs"

# Update CA certs on all nodes
declare -a hosts=("dvorak.maas" "dvorak-1-1.maas" "dvorak-1-2.maas" "dvorak-1-3.maas" "dvorak-1-4.maas" "dvorak-2-1.maas" "dvorak-2-2.maas" "dvorak-2-3.maas" "dvorak-2-4.maas" "brahms-02.maas" "brahms-03.maas" "brahms-04.maas" "brahms-05.maas" "tuleta.maas")
for h in "${hosts[@]}"
do
	echo $h
	scp ${CERTDIR}/dockerCA.crt ubuntu@${h}:
	ssh -t ubuntu@${h} "sudo mv ~/dockerCA.crt /usr/local/share/ca-certificates/"
	ssh -t ubuntu@${h} "sudo update-ca-certificates"
	ssh -t ubuntu@${h} "sudo service docker restart" 
done

# Redhat machines
declare -a hosts=("bach.maas")
for h in "${hosts[@]}"
do
	echo $h
	scp ${CERTDIR}/dockerCA.crt redhat@${h}:
	ssh -t redhat@${h} "sudo mv ~/dockerCA.crt /etc/pki/ca-trust/source/anchors/"
	ssh -t redhat@${h} "sudo update-ca-trust extract"
	ssh -t redhat@${h} "sudo service docker restart" 
done

