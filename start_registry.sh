#!/bin/bash

CERTDIR=/scratch/docker-registry/certs
REGISTRYDIR=/scratch/docker-registry/registry

docker run -d \
  --restart=always \
  --name docker-registry \
  -v "${CERTDIR}":/certs \
  -v "${REGISTRYDIR}":/var/lib/registry \
  -e REGISTRY_HTTP_ADDR=0.0.0.0:443 \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
  -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
  -p 443:443 \
  registry:2

