#!/bin/bash

CERTDIR="/scratch/docker-registry/certs"
DOMAIN="dvorak.maas"

rm -rf ${CERTDIR}/*

# Create a certification authority
openssl genrsa -out ${CERTDIR}/dockerCA.key 2048

# Create root certificate
openssl req -x509 -new -nodes -key ${CERTDIR}/dockerCA.key -days 10000 -out ${CERTDIR}/dockerCA.crt

# Generate server key
openssl genrsa -out ${CERTDIR}/domain.key 2048

# Request new certificate
openssl req -new -key ${CERTDIR}/domain.key -out ${CERTDIR}/${DOMAIN}.csr

# Sign certificate
openssl x509 -req -in ${CERTDIR}/${DOMAIN}.csr -CA ${CERTDIR}/dockerCA.crt -CAkey ${CERTDIR}/dockerCA.key -CAcreateserial -out ${CERTDIR}/domain.crt -days 10000

