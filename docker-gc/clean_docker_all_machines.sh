#!/bin/bash

# Clean Intel Machines
declare -a hosts=("dvorak.maas" "dvorak-1-1.maas" "dvorak-1-2.maas" "dvorak-1-3.maas" "dvorak-1-4.maas" "dvorak-2-1.maas" "dvorak-2-2.maas" "dvorak-2-3.maas" "dvorak-2-4.maas")
for h in "${hosts[@]}"
do
	echo $h
	ssh -t ubuntu@$h "docker pull dvorak.maas/docker-gc:latest_x86_64"
	ssh -t ubuntu@$h "docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -v /etc:/etc -e REMOVE_VOLUMES=1 dvorak.maas/docker-gc:latest_x86_64"
	ssh -t ubuntu@$h "docker system prune --all --force --volumes" 
done


# Clean IBM Machines
declare -a hosts=("brahms-02.maas" "brahms-03.maas" "brahms-04.maas" "brahms-05.maas" "tuleta.maas")
for h in "${hosts[@]}"
do
	echo $h
	ssh -t ubuntu@$h "docker pull dvorak.maas/docker-gc:latest_ppc64le"
	ssh -t ubuntu@$h "docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -v /etc:/etc -e REMOVE_VOLUMES=1 dvorak.maas/docker-gc:latest_ppc64le"
	ssh -t ubuntu@$h "docker system prune --all --force --volumes" 
done
# Clean IBM Machines - Redhat
declare -a hosts=("bach.maas")
for h in "${hosts[@]}"
do
	echo $h
	ssh -t redhat@$h "docker pull dvorak.maas/docker-gc:latest_ppc64le"
	ssh -t redhat@$h "docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -v /etc:/etc -e REMOVE_VOLUMES=1 dvorak.maas/docker-gc:latest_ppc64le"
	ssh -t redhat@$h "docker system prune --all --force" 
done

